﻿using HL7.Serializer.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HL7.Serializer
{
    class Program
    {
        static void Main(string[] args)
        {

            var obj = new TestClass
            {
                EncodingCharacters = @"^~\&",
                Separator = "\\|",
                FacilityId = "FACID",
                OID = "OID",
                PatientId = "PatientID",
                SampleProp = "sample value",
                SampleProp2 = "sample value 2",
                SampleProp3 = "sample value 3",
                MSH5 = "msh5"
            };

            var properties = obj.GetType().GetProperties().Where(prop => prop.IsDefined(typeof(MappingAttribute), false));
            var attributes = properties
                .Select(p => new { p.Name, Attribute = (MappingAttribute)p.GetCustomAttributes(typeof(MappingAttribute), false).First() })
                .OrderBy(p => p.Attribute.Position)
                .GroupBy(p => p.Attribute.Segment).ToList();

            var lp1 = 0;
            var lp2 = 0;
            var lp3 = 0;
            var message = new List<Segment>();
            foreach (var item in attributes)
            {
                //add segment
                var segment = new Segment(item.Key);
                var items = item.AsQueryable()
                    .Select(p => new FieldDescriptor()
                    {
                        Name = p.Name,
                        Position = p.Attribute.Position//.Split('.', StringSplitOptions.None)[0]
                    }).ToList();

                
                segment.AddFields(items, obj);
                //this is a segment with only single position definitions (MSH-1 rather than MSH-1.2 or MSH-1.2.3)
                var test1 = segment.ToStringV1();
                message.Add(segment);
            }


            Console.Out.WriteLine();

        }

    }
}
