﻿namespace HL7.Serializer
{
    public class TestClass
    {
        [MappingAttribute("MSH", "3.3.3")]
        public string SampleProp3 { get; set; }

        [MappingAttribute("MSH", "3.2")]
        public string OID { get; set; }

        [MappingAttribute("MSH", "3.3.2")]
        public string SampleProp2 { get; set; }

        [MappingAttribute("MSH", "2")]
        public string EncodingCharacters { get; set; }

        [MappingAttribute("PID", "1")]
        public string PatientId { get; set; }

        [MappingAttribute("MSH", "3.1")]
        public string FacilityId { get; set; }

        [MappingAttribute("MSH", "3.3.1")]
        public string SampleProp { get; set; }

        [MappingAttribute("MSH", "1")]
        public string Separator { get; set; }

        [MappingAttribute("MSH", "5")]
        public string MSH5 { get; set; }
    }
}
