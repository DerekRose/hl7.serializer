﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HL7.Serializer
{
    [AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = true)]
    public class MappingAttribute : Attribute
    {
        public string Segment { get; set; }
        public string Position { get; set; }
        public int PositionCount { get; set; }

        public MappingAttribute(string segment, string position)
        {
            Segment = segment;
            Position = position;
            PositionCount = position.Split('.').Length;
        }
    }
}
