﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HL7.Serializer.Models
{
    public class Segment
    {
        private string _header;
        private Field[] _fields;

        public Segment(string header)
        {
            _header = header;
        }

        public string ToStringV1()
        {
            return string.Join('|', _fields.Select(p => p.Value));
        }
        public void AddFields<T>(IEnumerable<FieldDescriptor> fields, T obj)
        {
            if (fields == null || !fields.Any()) return;
            var maxValue = int.Parse(fields.Max(p => p.Position.Split('.')[0]));
            if (_fields == null)
            {
                var i = 0;
                _fields = new Field[maxValue].Select(p => new Field((++i).ToString(), "") { }).ToArray();
            }
            else if (_fields.Length < maxValue)
            {
                var diff = Math.Abs(_fields.Length - maxValue);
                var i = 0;
                var newFields = new Field[diff].Select(p => new Field((++i + _fields.Length).ToString(), "") { }).ToArray();
                var oldLength = _fields.Length;
                Array.Resize(ref _fields, oldLength + newFields.Length);
                Array.Copy(newFields, 0, _fields, oldLength, newFields.Length);
            }
            var items = fields.ToArray();
            var lp1 = 0;
            var lp2 = 0;
            var lp3 = 0;

            for (int i = 0; i < fields.Count(); i++)
            {
                var positions = items[i].Position.Split('.');
                var currentPos1 = int.Parse(positions[0]) - 1;
                var currentPos2 = 0;
                var currentPos3 = 0;
                var value = string.Empty;
                if (positions.Length == 1)
                {
                    while (lp1 != currentPos1)
                    {
                        _fields[lp1].Value = string.Empty;

                        lp1++;
                    }
                    var field = typeof(T).GetProperty(items[i].Name);
                    _fields[lp1].Value = (string)field.GetValue(obj);
                    lp1++;
                    //lp1 = currentPos1;

                }
                else if (positions.Length == 2)
                {
                    while (lp2 != currentPos2)
                    {
                        _fields[lp1].Value += ("^" + string.Empty);

                        lp2++;
                    }
                    //append to current lp1 w/^
                    //check next position and incriment if need be
                }
                else if (positions.Length == 3)
                {
                    while (lp3 != currentPos3)
                    {
                        _fields[lp1].Value += ("~" + string.Empty);

                        lp2++;
                    }
                    //append current lp1 w/~
                    //check next position and incriment if need be
                }
            }
        }
    }
}
