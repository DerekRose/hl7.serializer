﻿namespace HL7.Serializer.Models
{
    public class FieldDescriptor
    {
        public string Name { get; set; }
        public string Position { get; set; }

        public FieldDescriptor()
        {

        }
    }
}
