﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HL7.Serializer.Models
{
    public class ListField : Field
    {
        private readonly List<string> _values;
        public ListField(string position) : base(position, string.Empty)
        {
            _values = new List<string>();
        }

        public void AddField(FieldDescriptor field)
        {

        }

        public string ToStringV231()
        {
            return "";
        }
    }
}
