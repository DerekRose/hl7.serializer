﻿namespace HL7.Serializer.Models
{
    public class Field
    {
        public string Position { get; set; }
        public string Value { get; set; }

        public Field(string position, string value)
        {
            Position = position;
            Value = value;
        }

    }
}
